
import _envConfig from '@/config/env.dev.js'
import _prodConfig from '@/config/env.prod.js'

function env_config(){
	if (process.env.NODE_ENV === "development") {
		return _envConfig
	} else if (process.env.NODE_ENV === "production") {  
		return _prodConfig
	}  
}

const config = env_config()
	
export default {
	config
}