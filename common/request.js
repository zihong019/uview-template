import Request from '../libs/luch-request'
import _envConfig from '@/config/env.dev.js'
import _prodConfig from '@/config/env.prod.js'

// 环境变量
// console.log(process.env)

/**
 * @description: get env config
 * @param {*}
 * @return {*}
 */
function env_config(){
	if (process.env.NODE_ENV === "development") {
		console.log('env ==> development',_envConfig.baseUrl)
		return _envConfig
	} else if (process.env.NODE_ENV === "production") {  
		return _prodConfig
	}  
}


const configUrl = () =>{
	let url = ''
	try {
		url = uni.getStorageSync('baseURL');
	} catch(e){
		// TODO handle the exception
		
	}
	return configUrl
}


/**
 * @description: get token
 * @param {*}
 * @return {*}
 */
const getTokenStorage = () => {
  let token = ''
  try {
    token = uni.getStorageSync('token')
  } catch (e) {
    //TODO handle the exception
  }
  return token
}


/**
 * @description: get session
 * @param {*}
 * @return {*}
 */
const getSessionidStorage = () => {
  let sessionid = ''
  try {
    sessionid = uni.getStorageSync('session')
  } catch (e) {
    //TODO handle the exception
  }
  return sessionid
}


/**
 * @description: get csrftoken
 * @param {*}
 * @return {*}
 */
const getCsrftokenStorage = () => {
  let csrftoken = ''
  try {
    csrftoken = uni.getStorageSync('csrftoken')
  } catch (e) {
    //TODO handle the exception
  }
  return csrftoken
}

const http = new Request()
const _config = env_config()
// console.log('base_url ==>',_config.baseUrl)


// 全局配置
http.setConfig((config) => {
  config.baseURL = _config.baseUrl 
  config.header = {
    ...config.header
  }
  return config
})

 // 请求之前拦截器。可以使用async await 做异步操作
http.interceptors.request.use((config) => {
	if(getTokenStorage()!==''){
		config.header = {
			...config.header,
			// Authorization:'JWT ' + getTokenStorage()
			// 'X-CSRFToken': getCsrftokenStorage()
			// 'Content-Type':'application/x-www-form-urlencoded'
		}
	}else{
		config.header = {
			...config.header
			}	
	}
	return config
}, (config) => {
  return Promise.reject(config)
})

// 请求之后拦截
// 必须使用异步函数，注意
http.interceptors.response.use((response) => { 
console.log('success response ==>',response)
// 错误处理
  // if (response.data.code !== 200) { 
  //   return Promise.reject(response)
	// 	uni.reLaunch({
	// 		url:'@/page/login/login'
	// 	})
  // }
  return response
}, (error) => { // 请求错误做点什么。可以使用async await 做异步操作
  console.log('error ==>',error)
  // if(error.errMsg ==='request:fail timeout'){
	//   console.log('账户验证信息已经过期/网络不好')
	//   uni.showToast({
	//   	title: '账户验证信息已经过期,请重新登陆！！！/网络不好，请检查网络！！！',
	//   	icon: 'none'
	//   })
	//   console.log('token已经过期/网络不好')
	//   uni.reLaunch({
	//   	url:'/pages/login/login'
	//   }) 
  // }
  return response
})

export {
  http
}
