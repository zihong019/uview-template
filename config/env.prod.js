/*
 * @Author: zihong
 * @Date: 2021-06-30 23:56:23
 * @LastEditTime: 2021-07-01 00:06:45
 * @LastEditors: Please set LastEditors
 * @Description: prod config
 * @FilePath: /uview-template/config/env.prod.js
 */
const baseUrl = ''

export default {
	baseUrl,
}
