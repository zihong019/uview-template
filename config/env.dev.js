/*
 * @Author: zihong
 * @Date: 2021-06-30 23:56:23
 * @LastEditTime: 2021-07-01 00:06:45
 * @LastEditors: Please set LastEditors
 * @Description: dev config
 * @FilePath: /uview-template/config/env.dev.js
 */

const baseUrl = ''

export default {
	baseUrl,
}
