/*
 * @Author: zihong
 * @Date: 2021-06-30 23:56:23
 * @LastEditTime: 2021-07-01 00:06:45
 * @LastEditors: Please set LastEditors
 * @Description: common api
 * @FilePath: /uview-template/api/common.js
 */
import { http } from '@/common/request.js'


/**
 * @description: get all
 * @param {string} url
 * @param {object} params
 * @return {} promise
 */
export function getAllList(url,params={}){
  return http.get(url,params)
}

/**
 * @description: get item
 * @param {string} url
 * @param {object} params
 * @return {} promise
 */
 export function getItem(url,params={}){
  return http.get(url,params)
}

/**
 * @description: post form
 * @param {string} url
 * @param {object} data
 * @return {} promise
 */
 export function postForm(url,data){
  return http.get(url,data)
}